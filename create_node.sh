#!/bin/bash

. env.sh

COMMENT="""
What would you like to do? (default = stats)
1. Show network stats
2. Manage existing genesis
3. Track new remote server
4. Deploy network components
> 2

1. Modify existing fork rules
2. Export genesis configurations
3. Remove genesis configuration
> 2

Which folder to save the genesis specs into? (default = current)
Will create privnet.json, privnet-aleth.json, privnet-harmony.json, privnet-parity.json
> node-miner-1
INFO [04-19|05:41:48.541] Saved native genesis chain spec path=genesis.json/privnet.json
INFO [04-19|05:41:48.544] Saved genesis chain spec client=aleth path=genesis.json/privnet-aleth.json
INFO [04-19|05:41:48.546] Saved genesis chain spec client=parity path=genesis.json/privnet-parity.json
INFO [04-19|05:41:48.547] Saved genesis chain spec client=harmony path=genesis.json/privnet-harmony.json
"""

if [ "z$1" = "z" ]
then
	echo "$0 <node name>"
	exit
fi

# Delete the existing container
rm -rf $NODEDATA/node-$1

$REPO/build/bin/puppeth --network=$NETWORK_NAME << EOF
2
2
$NODEDATA/node-$1
EOF
