#!/bin/bash

. env.sh

# Build all from repo
sh -c 'cd $REPO && make -j`nproc` all'

# Build geth container
docker image build --rm -t $GETH_IMAGE --build-arg REPO=$REPO .
