#!/bin/bash

. env.sh

# $1: miner name
# $2: wallet address

miner_ip=$(docker inspect -f "{{ .NetworkSettings.IPAddress }}" $1)
miner_addr="$miner_ip:$GETH_RPC_PORT"

res=$(curl -s -X POST -H 'Content-Type: application/json' --data '{"jsonrpc":"2.0","method":"personal_unlockAccount","params":['$2', "'$MINER_PASSWORD'", 0],"id":'$NETWORK_ID'}' --max-time 30 $miner_addr)

if [ "z$res" == "z" ]
then
	echo "Connection timeout of 30 seconds for $1"
	exit
elif [ "z$(echo $res | jq '.result')" != "ztrue" ]
then
	echo "Wrong response: $(echo $res | jq '.error')"
	exit
fi
