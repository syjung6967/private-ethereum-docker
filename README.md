Tested on Ubuntu 18.04.2 LTS.

Current hierarchy for Docker containers:
````
bootnode
|- miner-1
|- miner-2
|- ...
+- miner-n
````

First of all, check prerequisites for Ethereum and Docker.

Next, clone the Ethereum repository into geth-repo folder.
````
$ git clone --depth 1 -b v1.8.24 https://github.com/ethereum/go-ethereum geth-repo
````

Run the scripts in the following order:
1. build_docker_image.sh
2. create_genesis_json.sh
3. create_node.sh (for bootnode and miners)
4. run_bootnodes.sh (for bootnode)
5. run_miner.sh (for each miner)
6. kill_nodes.sh

You can execute **run_nodes.sh** to run step 3 to step 5 fast.

You can check geth for each container, using 'geth attach $NETWORK_NAME/node-miner-xxx/geth.ipc'.
