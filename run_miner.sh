#!/bin/bash

. env.sh

MINER_NAME="$1"
MINER_DATA="$NODEDATA/node-$MINER_NAME"

if [ "z$MINER_NAME" == "z" ]
then
	echo "$0 <miner name>"
	exit
fi

if [ ! -d $MINER_DATA ]
then
	echo "$MINER_DATA does not exist"
	echo "Run create_node.sh first"
	exit
fi

# Read bootnode list
if [ ! -f bootnode.list ]
then
	echo "Run bootnode first"
	exit
fi
bootnode_enodes=$(echo -n $(cat bootnode.list) | tr '\n' ',')

# Run miner container
docker run -d --cpus="$CONTAINER_CPUS" -v $MINER_DATA:/root/.ethereum --name $MINER_NAME --rm $GETH_IMAGE

# Init blockchain from genesis.json
docker exec $MINER_NAME geth init /root/.ethereum/privnet.json

# Create miner account (Etherbase)
# Original output: 'Address: {cba5b07b6e8dd32e080f5177b3a89b5df0d40047}'
# Modified output: '0xcba5b07b6e8dd32e080f5177b3a89b5df0d40047'
ebase_addr=$(docker exec $MINER_NAME sh -c 'echo '$MINER_PASSWORD' > /tmp/pass && geth account new --lightkdf --password /tmp/pass')
ebase_addr=$(echo $ebase_addr | awk '{ print "0x"substr($2, 2, length($2)-2) }')

# Start mining
docker exec $MINER_NAME geth \
	--identity "$NETWORK_NAME-$MINER_NAME" \
	--syncmode $MINER_SYNCMODE \
	--lightkdf \
	--rpc --rpcport $GETH_RPC_PORT --rpcaddr "0.0.0.0" --rpccorsdomain "*" \
	--rpcapi "admin,db,eth,debug,miner,net,shh,txpool,personal,web3" \
	--bootnodes $bootnode_enodes \
	--mine --miner.threads 1 --miner.etherbase $ebase_addr \
	--cache $MINER_CACHESIZE \
	--verbosity $MINER_VERBOSITY
