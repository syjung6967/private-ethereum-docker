#!/bin/bash

. env.sh

if [ "z$1" == "z" ]
then
	echo "$0 <the number of miners> [<omit='off'>]"
	exit
fi

case $1 in
	''|*[!0-9]*) echo "Input must be number"; exit ;;
esac

omit=
if [ "z$2" == "zoff" ]
then
	echo "Omit node creation process"
	omit=$2
else
	echo "Create nodes for bootstrap and miner"
fi

if [ "z$omit" != "zoff" ]
then
	./create_node.sh bootnode
fi
./run_bootnodes.sh

for i in `seq 1 $1`
do
	(
	 if [ "z$omit" != "zoff" ]
	 then
		 ./create_node.sh miner-$i > /dev/null
	 fi
	 ./run_miner.sh miner-$i
	) &
done

echo -e ${RED}"Done"${NC}
