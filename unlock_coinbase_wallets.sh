#!/bin/bash

. env.sh

for i in `docker ps -a | grep $GETH_IMAGE | awk -F' ' '{ print $NF }'`
do
	case "$i" in
		miner-[0-9]*) echo -n "$i: " ;;
		*) continue ;;
	esac

	miner_ip=$(docker inspect -f "{{ .NetworkSettings.IPAddress }}" $i)
	miner_addr="$miner_ip:$GETH_RPC_PORT"

	coinbase_addr=$(curl -s -X POST -H 'Content-Type: application/json' --data '{"jsonrpc":"2.0","method":"eth_coinbase","params":[],"id":'$NETWORK_ID'}' $miner_addr | jq '.result')

	echo $coinbase_addr

	./unlock_wallet.sh $i $coinbase_addr &
done

wait

echo -e ${RED}"Done"${NC}
