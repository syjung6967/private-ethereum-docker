#!/bin/bash

. env.sh

# Terminate geth safe to flush its cache completely
for i in `docker ps -a | grep $GETH_IMAGE | awk -F' ' '{ print $NF }'`
do
	echo "Kill container $i"

	# Kill container after changing the ownership from root into runner
	USER_ID=$(id -u)

	(docker exec $i sh -c '
	 pkill -INT geth &&
	 chown -R '$USER_ID:$USER_ID' /root/.ethereum && sync
	 '; docker kill $i) &
done

wait

echo -e ${RED}"Done"${NC}
echo ""
echo "Current running containers:"
docker ps -a
