#!/bin/bash

# Color map
export NC='\033[0m'              # Text Reset
# Bold Colors
export BLACK='\033[1;30m'        # Black
export RED='\033[1;31m'          # Red
export GREEN='\033[1;32m'        # Green
export YELLOW='\033[1;33m'       # Yellow
export BLUE='\033[1;34m'         # Blue
export PURPLE='\033[1;35m'       # Purple
export CYAN='\033[1;36m'         # Cyan
export WHITE='\033[1;37m'        # White

# Private Ethereum network name
export NETWORK_NAME="privnet"

# Private Ethereum network ID
export NETWORK_ID="57763"

# Directory mounted on Docker containers must be specified as absolute path
export NODEDATA="`pwd -P`/$NETWORK_NAME"

# Ethereum container name and tag
export GETH_IMAGE="geth:test"

# Geth RPC port
export GETH_RPC_PORT="8545"

# Bootnode port
export BOOTNODE_PORT="30301"

# Limit for CPU resources on each container
export CONTAINER_CPUS="0.5"

# Megabytes of memory allocated to internal caching (default: 1024)
export MINER_CACHESIZE="32"

# Blockchain sync mode (default: "fast")
# "full": block header + block data + full validation
# "fast": block header + block data + validate for last 1k transactions
# "light": Current state + Asks nodes for as its need.
#          (request missing blocks from other nodes)
export MINER_SYNCMODE="fast"

# Miner password
export MINER_PASSWORD="minerpass"

# Logging verbosity for miner
# 0=silent, 1=error, 2=warn, 3=info, 4=debug, 5=detail (default: 3)
export MINER_VERBOSITY="2"

# Repository for geth
export REPO="./geth-repo"
