#!/bin/bash

. env.sh

# Run bootstrap node (bootnode)
docker run -d --cpus="$CONTAINER_CPUS" -v $NODEDATA/node-bootnode:/root/.ethereum --name bootnode --rm $GETH_IMAGE

# Create the private key for bootnode
docker exec bootnode sh -c 'KEY=/root/.ethereum/boot.key && bootnode -genkey $KEY'

# Run bootnode
##
## Protocol option:
## P2P v4 discovery bootstrap (light server, full nodes, default)
## P2P v5 discovery bootstrap (light server, light nodes)
##
## Currently v5 protocol has some problem:
## For 30303 port, bootnode cannot retrieve nodes.
## For the other ports, bootnode prints "bad packet" error,
## due to wrong packet header.
##BOOTNODE_V5="-v5"
docker exec bootnode sh -c 'KEY=/root/.ethereum/boot.key && bootnode -nodekey $KEY -writeaddress && bootnode -nodekey $KEY -verbosity 9 -addr :'$BOOTNODE_PORT' '$BOOTNODE_V5 &

# Wait until bootnode is up and get its IP
bootnode_ip=""
while [ "z$bootnode_ip" == "z" ]
do
	echo "Wait until bootnode is up"
	sleep 1
	bootnode_ip=$(docker inspect -f "{{ .NetworkSettings.IPAddress }}" bootnode)
done

# Get the bare enode address for the bootnode
bootnode_enode_bare=$(docker exec bootnode sh -c 'KEY=/root/.ethereum/boot.key && bootnode -nodekey $KEY -writeaddress')

# Get the complete enode address for the bootnode
bootnode_enode="enode://$bootnode_enode_bare@$bootnode_ip:$BOOTNODE_PORT"

# Output the bootnode enode list as a file
echo "Save bootnode enode list in bootnode.list"

echo "$bootnode_enode" > bootnode.list
