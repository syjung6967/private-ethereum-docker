# Build Geth in a stock Go builder container
FROM golang:1.12-alpine as builder

RUN apk add --no-cache make gcc musl-dev linux-headers

ARG REPO=geth-repo
ADD $REPO /go-ethereum
RUN cd /go-ethereum && make all

# Pull all binaries into a second stage deploy alpine container
FROM alpine:latest

RUN apk add --no-cache ca-certificates openssh
COPY --from=builder /go-ethereum/build/bin/* /usr/local/bin/

# SSH configuration
RUN mkdir /var/run/sshd
RUN echo 'root:mypass' | chpasswd
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN ssh-keygen -A
EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]

EXPOSE 8545 8546 30303 30303/udp
