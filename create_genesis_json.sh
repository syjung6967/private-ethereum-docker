#!/bin/bash

. env.sh

COMMENT="""
+-----------------------------------------------------------+
| Welcome to puppeth, your Ethereum private network manager |
|                                                           |
| This tool lets you create a new Ethereum network down to  |
| the genesis block, bootnodes, miners and ethstats servers |
| without the hassle that it would normally entail.         |
|                                                           |
| Puppeth uses SSH to dial in to remote servers, and builds |
| its network components out of Docker containers using the |
| docker-compose toolset.                                   |
+-----------------------------------------------------------+

Please specify a network name to administer (no spaces, hyphens or capital letters please)
> privnet

Sweet, you can set this via --network=privnet next time!

INFO [04-19|04:56:23.189] Administering Ethereum network name=privnet
WARN [04-19|04:56:23.189] No previous configurations found path=/home/syjung/.puppeth/privnet

What would you like to do? (default = stats)
1. Show network stats
2. Configure new genesis
3. Track new remote server
4. Deploy network components
> 2

What would you like to do? (default = create)
1. Create new genesis from scratch
2. Import already existing genesis
> 1

Which consensus engine to use? (default = clique)
1. Ethash - proof-of-work
2. Clique - proof-of-authority
> 1

Which accounts should be pre-funded? (advisable at least one)
> 0x

Should the precompile-addresses (0x1 .. 0xff) be pre-funded with 1 wei? (advisable yes)
>

Specify your chain/network ID if you want an explicit one (default = random)
>
INFO [04-19|04:57:35.965] Configured new genesis block
"""

# Delete the existing network
rm -rf "$HOME/.puppeth/$NETWORK_NAME"

$REPO/build/bin/puppeth --network=$NETWORK_NAME << EOF
2
1
1


$NETWORK_ID
EOF
